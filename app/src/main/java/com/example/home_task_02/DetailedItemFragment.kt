package com.example.home_task_02

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import com.example.home_task_02.databinding.FragmentDetailedItemBinding
import kotlin.system.exitProcess

class DetailedItemFragment : Fragment(), OnItemClickListener {

    private lateinit var binding: FragmentDetailedItemBinding
    private lateinit var listener: OnDetailedItemFragmentDataListener
    private val dataModel: DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailedItemBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (view.context is OnDetailedItemFragmentDataListener) {
            listener = view.context as OnDetailedItemFragmentDataListener
        }
        dataModel.dataItem.observe(activity as LifecycleOwner) {
            binding.imageView2.setImageResource(it.imageId)
            binding.tv2Title.text = it.title
            binding.tv2Description.text = it.description
        }
        initToolbar()
        binding.btnCloseApp.setOnClickListener {
            exitProcess(-1)
        }
    }

    private fun initToolbar() {
        with(binding.toolbar) {
            title = "Back"
            setNavigationOnClickListener {
                listener.onOpenRvFragment()
            }
        }
    }


    companion object {

        @JvmStatic
        fun newInstance() = DetailedItemFragment()
    }

    override fun onItemClick(item: Item, position: Int) {

    }
}
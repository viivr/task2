package com.example.home_task_02

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.home_task_02.databinding.FragmentRvBinding

internal var itemsList: ArrayList<Item> = ArrayList()

class RvFragment() : Fragment(), OnItemClickListener {
    private lateinit var binding: FragmentRvBinding
    private val adapter = ItemAdapter(itemsList, this)
    private lateinit var listener: OnRvFragmentDataListener
    private val dataModel: DataModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRvBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (view.context is OnRvFragmentDataListener) {
            listener = view.context as OnRvFragmentDataListener
        }
        setInitialData()
        init()
    }

    private fun setInitialData() {
        for (i in 0..999) {
            itemsList.add(Item(R.drawable.self_improvement_black_24dp, "Title ${i + 1}",
                "Description ${i + 1}"))
        }
    }

    private fun init() {
        binding.apply {
            rvList.layoutManager = GridLayoutManager(this@RvFragment.context, 1)
            rvList.adapter = adapter
        }
    }
    companion object {
        @JvmStatic
        fun newInstance() = RvFragment()
    }

    override fun onItemClick(item: Item, position: Int) {
        dataModel.dataItem.value = item
        listener.onOpenDetailedFragment()
    }
}
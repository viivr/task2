package com.example.home_task_02

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class DataModel : ViewModel() {
    val dataItem: MutableLiveData<Item> by lazy {
        MutableLiveData<Item>()
    }
}
package com.example.home_task_02

data class Item(val imageId: Int, val title: String, val description: String)

package com.example.home_task_02

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.home_task_02.databinding.RvitemBinding

class ItemAdapter(itemsList: ArrayList<Item>, var clickListener: OnItemClickListener) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val binding = RvitemBinding.bind(item)
        fun bind(itm: Item, action: OnItemClickListener) = with(binding) {
            imageView.setImageResource(itm.imageId)
            tvTitle.text = itm.title
            tvDescription.text = itm.description
            itemView.setOnClickListener {
                action.onItemClick(itm, adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rvitem, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemsList[position], clickListener)
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

}

interface OnItemClickListener {
    fun onItemClick(item: Item, position: Int)
}
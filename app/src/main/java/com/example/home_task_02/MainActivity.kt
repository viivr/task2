package com.example.home_task_02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.home_task_02.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OnRvFragmentDataListener, OnDetailedItemFragmentDataListener {
    lateinit var binding: ActivityMainBinding
    private val dataModel: DataModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        openRvFragment()
        dataModel.dataItem.observe(this) {
            var title = it.title
            var description = it.description
            var imageID = it.imageId
        }
    }

    private fun openRvFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainerView, RvFragment.newInstance())
            .commit()
    }

    private fun openDetailedItemFragment() {
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(R.id.fragmentContainerView, DetailedItemFragment.newInstance())
            .commit()
    }

    override fun onOpenDetailedFragment() {
        openDetailedItemFragment()
    }

    override fun onOpenRvFragment() {
        supportFragmentManager.popBackStack()
    }
}